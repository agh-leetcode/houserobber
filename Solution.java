class Solution {

    public static void main(String args[]){
        Solution solution = new Solution();
        int res = solution.rob(new int[]{2,7,9,3,1});
        System.out.println(res);
    }
    
    private int rob(int[] nums) {
        if(nums.length==0)
            return 0;
        
        int [] temp = new int[nums.length+1];
        temp[0] = 0;
        temp[1] = nums[0];
        
        for(int i = 1; i < nums.length; i++){
            int val = nums[i];
            temp[i+1] = Math.max(temp[i], temp[i-1] + val);
        }
            
        return temp[nums.length];
    }
    
    /*  private int rob(int[] num) {
        int last = 0;
        int now = 0;
        int tmp;
        for (int n :num) {
            tmp = now;
            now = Math.max(last + n, now);
            last = tmp;
        }
        return now;        
    }
    */
}